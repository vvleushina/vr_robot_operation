#include "ConnectionManager.h"
#include "Containers/UnrealString.h"
#include "Containers/Array.h"
#include "Kismet/GameplayStatics.h"

#include "miem_ki_ue4.h"

// Sets default values
AConnectionManager::AConnectionManager()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.SetTickFunctionEnable(true);
	//PrimaryActorTick.bTickEvenWhenPaused = true;
	//PrimaryActorTick.TickGroup = TG_PrePhysics;
	PrimaryActorTick.TickInterval = 0.5;
	this->SetActorTickEnabled(true);
	//When the object is constructed, Get the HTTP module
	Http = &FHttpModule::Get();
}

// Called when the game starts or when spawned
void AConnectionManager::BeginPlay()
{
	Super::BeginPlay();
	callPeriod = 0.1;
	lastCallTime = 0;
	//GEngine->AddOnScreenDebugMessage(1, 2.0f, FColor::Green, FString("Object created!"));
	UE_LOG(LogTemp, Warning, TEXT("AConnectionManager created."));

	GetWorld()->GetTimerManager().SetTimer(tmHandle, this, &AConnectionManager::timerFunc, 0.2, true,0);
}

/*Http call*/
void AConnectionManager::MyHttpCall()
{
	TSharedRef<IHttpRequest> Request = Http->CreateRequest();
	Request->OnProcessRequestComplete().BindUObject(this, &AConnectionManager::OnResponseReceived);
	//This is the url on which to process the request
	Request->SetURL("http://172.20.203.51:29025");
	Request->SetVerb("GET");
	Request->SetHeader(TEXT("User-Agent"), "X-UnrealEngine-Agent");
	Request->SetHeader("Content-Type", TEXT("application/json"));
	Request->ProcessRequest();
}

/*Assigned function on successfull http call*/
void AConnectionManager::OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful)
{
	parseMessage(Response->GetContentAsString());
}

void AConnectionManager::timerFunc() {
	MyHttpCall();
}

FString getRobotNameFromLink(const FString& link) {
	int32 div_pos = 0;
	auto tmp = link;
	tmp.RemoveFromStart("/");
	if (tmp.FindChar('/', div_pos)) {
		//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, tmp.Mid(0, div_pos));
		return tmp.Mid(0, div_pos);
	}
	else {
		return link;
	}
}

void AConnectionManager::jsonProcessNavMsgsOdometry(const TSharedPtr<FJsonObject>&obj, const FString&name) {
	// get name of the robot
	// get pointer to position of the robot
	auto p_pos = obj->GetObjectField("pose")->GetObjectField("pose");
	auto p_position = p_pos->GetObjectField("position");
	auto p_orient = p_pos->GetObjectField("orientation");
	// calculate position of the model
	auto pos_x = p_position->GetNumberField("x");
	auto pos_y = p_position->GetNumberField("y");
	// calculate orientation of the model
	FRotator orient(FQuat(p_orient->GetNumberField("x"),
		p_orient->GetNumberField("y"),
		p_orient->GetNumberField("z"),
		p_orient->GetNumberField("w")));
	// send position to the robot (the coordinate system in UE4 is left-handed)
	moveRobot(name, -pos_x * 100, pos_y * 100, 180 - orient.Yaw);
	//char buf[100];
	//sprintf(buf, "%.2f %.2f , %.1f", -pos_x * 100, pos_y * 100, 180 - orient.Yaw);
	//GEngine->AddOnScreenDebugMessage(100, 2.0f, FColor::Red, FString(buf));
}

void AConnectionManager::jsonProcessDiagnosticMsgsDiagnosticStatus(const TSharedPtr<FJsonObject>&obj) {
	// read information
	auto type = obj->GetIntegerField("level");
	auto message = obj->GetStringField("message");
	auto name = obj->GetStringField("name");

	FString robot = "main";
	int32 id_message = 1;

	auto values = obj->GetArrayField("values");
	for (auto &v : values) {
		auto elem = v->AsObject();
		auto key = elem->GetStringField("key");
		if (key == "robot") {
			robot = elem->GetStringField("value");
			continue;
		}
		if (key == "id_message") {
			id_message = elem->GetIntegerField("value");
			continue;
		}
	}
	// generate message according to parameters
	getErrorInformation(robot, name, id_message, type, message);
}

void AConnectionManager::jsonProcessOperationModeStatus(const TSharedPtr<FJsonObject>&obj) {
	// read information
	auto camera = obj->GetStringField("data");
	// generate message according to parameters
	getOperationMode(camera);
}

void AConnectionManager::jsonProcessDiagnosticMsgsValues(const TSharedPtr<FJsonObject>&obj) {
	// read level value
	auto level = obj->GetIntegerField("level");
	// generae message according to parameters
	switch (level) {
	case 0: sucMess(); break;
	case 1: warnMess(); break;
	case 2: errorMess(); break;
	case 3: fatalMess(); break;
	default: GEngine->AddOnScreenDebugMessage(10, 2.0f, FColor::Red, "Level value in diagnostic message is unknown");
	};
}

void AConnectionManager::jsonProcessSensorMsgsPointCloud(const TSharedPtr<FJsonObject>&obj) {
	// get array of points
	auto points_array = obj->GetArrayField("points");
	// for each point
	for (auto &p : points_array ) {
		auto point = p->AsObject();
		// get fields and process
		auto x = point->GetNumberField("x");
		auto y = point->GetNumberField("y");
		createSensorObject(-x * 100, y * 100);
	}
}

void AConnectionManager::jsonProcessDisplayAutoPath(const TSharedPtr<FJsonObject>&obj, bool flag, const FString&name) {
	// get array of points
	auto points_array = obj->GetArrayField("points");
	// clear old path
	clearOldPath(name, flag);
	// for each point
	for (auto &p : points_array) {
		auto point = p->AsObject();
		// get fields and process
		auto x = point->GetNumberField("x");
		auto y = point->GetNumberField("y");
		createPathObject(name, -x * 100, y * 100);
	}
}

void AConnectionManager::jsonProcessVisualizationMsgsMarkerArray(const TSharedPtr<FJsonObject>&obj) {
	// get array of markers
	auto marker_array = obj->GetArrayField("markers");
	// for each marker
	for (auto &p : marker_array) {
		// get pose
		auto p_position = p->AsObject()->GetObjectField("pose")->GetObjectField("position");
		// calculate position of the marker
		auto pos_x = p_position->GetNumberField("x");
		auto pos_y = p_position->GetNumberField("y");
		createHorizontalObstacle(-pos_x*100, pos_y*100);
	}
}


void AConnectionManager::parseMessage(FString data) {
	// split to separate lines
	TSharedPtr<FJsonObject> jsonObject = MakeShareable(new FJsonObject());
	TSharedRef< TJsonReader<> > jsonReader = TJsonReaderFactory<>::Create(data);
	bool clear_path = false;

	if (FJsonSerializer::Deserialize(jsonReader, jsonObject) && jsonObject.IsValid())
	{
		int msg_id = 1;
		
		for (auto it = jsonObject->Values.begin(); it != jsonObject->Values.end(); ++it) {
			auto topic_name = it->Key;
			auto data_field = it->Value->AsObject()->GetObjectField("data");
			auto type = it->Value->AsObject()->GetStringField("type");
			auto robot_name = getRobotNameFromLink(topic_name);
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, type);

			if (type == "nav_msgs/Odometry") {
				jsonProcessNavMsgsOdometry(data_field, robot_name);
			}
			if (type == "diagnostic_msgs/DiagnosticStatus") {
				jsonProcessDiagnosticMsgsValues(data_field);
				jsonProcessDiagnosticMsgsDiagnosticStatus(data_field);
			}
			if (type == "sensor_msgs/PointCloud") {
				if (topic_name.Contains("obstacle_points"))
					jsonProcessSensorMsgsPointCloud(data_field);
				if (topic_name.Contains("correct_path")) {
					clear_path = true;
					jsonProcessDisplayAutoPath(data_field, clear_path, robot_name);
				}
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, topic_name);
			}
			if (type == "visualization_msgs/MarkerArray") {
				jsonProcessVisualizationMsgsMarkerArray(data_field);
			}
			if (type == "std_msgs/String") {
				jsonProcessOperationModeStatus(data_field);
				//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, topic_name);
			}
		}

		/*TSharedPtr<FJsonObject> jsonObj = JsonObject->GetObjectField("table");
		TArray<TSharedPtr<FJsonValue>> objArray = jsonObj->GetArrayField("rows");

		for (int32 i = 0; i < objArray.Num(); i++)
		{

			TArray<TSharedPtr<FJsonValue>> height = objArray[i]->AsArray();
			FString name = height[2]->AsString();

			UE_LOG(LogTemp, Warning, TEXT("Value I'm looking for %s"), *name);

		}*/
	}
}