// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "miem_ki_ue4.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, miem_ki_ue4, "miem_ki_ue4" );
