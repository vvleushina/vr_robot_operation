#pragma once
#include "CoreMinimal.h"
#include "Classes/Components/AudioComponent.h"
#include "Message.generated.h"

USTRUCT(BlueprintType)
struct FMessage
{
    GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	FString name;
	UPROPERTY(BlueprintReadWrite)
	int type;
	UPROPERTY(BlueprintReadWrite)
	FString message;
};