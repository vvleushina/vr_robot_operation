#pragma once

#include<map>
#include<list>

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "Runtime/Online/HTTP/Public/Http.h"
#include "TimerManager.h"
#include "Dom/JsonObject.h"
#include "Serialization/JsonSerializer.h"
#include "ConnectionManager.generated.h"

using std::map;
using std::list;

UCLASS()
class MIEM_KI_UE4_API AConnectionManager : public AActor
{
	GENERATED_BODY()

public:

	/* The actual HTTP call */
	UFUNCTION()
		void MyHttpCall();

	/*Assign this function to call when the GET request processes sucessfully*/
	void OnResponseReceived(FHttpRequestPtr Request, FHttpResponsePtr Response, bool bWasSuccessful);

	// Sets default values for this actor's properties
	AConnectionManager();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	//UFUNCTION()
	//virtual void Tick(float DeltaTime) override;
	void timerFunc();
	void parseMessage(FString data);
	void processMapObstacle(const FString&str);
	void processRobotPos(const FString&str_pos, const FString&str_orient);
	void processSensorPoint(const FString&str_from, const FString&str_to);
	void processDiagnosticMessage(const FString&str);
	

	void jsonProcessNavMsgsOdometry(const TSharedPtr<FJsonObject>&obj, const FString&name);
	void jsonProcessSensorMsgsPointCloud(const TSharedPtr<FJsonObject>&obj);
	void jsonProcessDisplayAutoPath(const TSharedPtr<FJsonObject>&obj, bool flag, const FString&name);
	void jsonProcessVisualizationMsgsMarkerArray(const TSharedPtr<FJsonObject>&obj);
	void jsonProcessDiagnosticMsgsValues(const TSharedPtr<FJsonObject>&obj);
	void jsonProcessDiagnosticMsgsDiagnosticStatus(const TSharedPtr<FJsonObject>&obj);
	void jsonProcessOperationModeStatus(const TSharedPtr<FJsonObject>&obj);

	UFUNCTION(BlueprintImplementableEvent)
		void moveRobot(const FString& robot, float x, float y, float orient);
	UFUNCTION(BlueprintImplementableEvent)
		void createHorizontalObstacle(float x, float y);
	UFUNCTION(BlueprintImplementableEvent)
		void createSensorObject(float x, float y);
	UFUNCTION(BlueprintImplementableEvent)
		void createPathObject(const FString& robot, float x, float y);
	UFUNCTION(BlueprintImplementableEvent)
		void clearOldPath(const FString& robot, bool flag);
	UFUNCTION(BlueprintImplementableEvent)
		void fatalMess();
	UFUNCTION(BlueprintImplementableEvent)
		void errorMess();
	UFUNCTION(BlueprintImplementableEvent)
		void warnMess();
	UFUNCTION(BlueprintImplementableEvent)
		void sucMess();
	UFUNCTION(BlueprintImplementableEvent)
		void getErrorInformation(const FString& robot, const FString& name, int id_message, int type, const FString& message);
	UFUNCTION(BlueprintImplementableEvent)
		void getOperationMode(const FString& camera);
private:
	float lastCallTime;
	float callPeriod;
	FTimerHandle tmHandle;
	map<float, list<float>> old_obsts;
	FHttpModule * Http;
};
